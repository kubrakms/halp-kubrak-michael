﻿// -----------------------------------------------------------------------
// <copyright file="LogHelper.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.fpFormat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    /// <summary>
    /// Represent logining logic
    /// </summary>
    public static class LogHelper
    {
        private static string fileName = "log.txt";

        private static void Write(string fullMessage)
        {
            using (StreamWriter sw = File.AppendText(fileName))
            {
                sw.WriteLine(fullMessage);
            }
        }

        /// <summary>
        /// Method for setting file for log. Default value "log.txt"
        /// </summary>
        /// <param name="file">Name of log file</param>
        public static void SetLogFile(string file)
        {
            if (!File.Exists(file))
                using (File.Create(file)) { };
            fileName = file;
        }

        /// <summary>
        /// Write message with Error prefics
        /// </summary>
        /// <param name="message"> error message</param>
        public static void WriteError(string message)
        {
            Write(string.Format("Exception: {0} \r\n", message));
        }

        /// <summary>
        /// Write message with Info prefics
        /// </summary>
        /// <param name="message"> info message</param>
        public static void WriteInfo(string message)
        {
            Write(string.Format("Info: {0} \r\n", message));
        }

        /// <summary>
        /// Write message with Warning prefics
        /// </summary>
        /// <param name="message"> warning message</param>
        public static void WriteWarning(string message)
        {
            Write(string.Format("Warning: {0} \r\n", message));
        }


    }
}
