﻿// -----------------------------------------------------------------------
// <copyright file="TriangleData.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.HalpUtilitiesData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using geometry;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    /// <summary>
    /// Represent ftFormat Triangle
    /// </summary>
    [Serializable]
    [XmlRoot("Triangle")]
    public class TriangleData
    {
        /// <summary>
        /// represent Id of Triangle
        /// </summary>
        
        [XmlAttribute("Id")]
        public long Id { get; set; }
        /// <summary>
        /// represent Name of Triangle
        /// </summary>
     
       [XmlAttribute("Name")]
        public string Name { get; set;}
        /// <summary>
        /// represent X 3d coordinate
        /// </summary>
       
        [XmlElement("FirstPoint")]
        public PointWrapper First { get; set; }
        /// <summary>
        /// represent Y 3d coordinate
        /// </summary>

        [XmlElement("SecondPoint")]
        public PointWrapper Second { get; set; }
        /// <summary>
        /// represent Z 3d coordinate
        /// </summary>

        [XmlElement("ThirdPoint")]
        public PointWrapper Third { get; set; }


    }
}
