﻿// -----------------------------------------------------------------------
// <copyright file="BinarySerializer.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.HalpUtilitiesData.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;


    /// <summary>
    /// /// Implement ISerializer logic for binary format
    /// </summary>
    public class BinarySerializer : ISerializer
    {
        public void Serialize(IEnumerable<TriangleData> data, string fileName)
        {
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(ms, data);
            File.WriteAllBytes(fileName, ms.ToArray());
        }

        public IEnumerable<TriangleData> Deserialize(string fileName)
        {
            BinaryFormatter bf = new BinaryFormatter();
            IEnumerable<TriangleData> halpDataList = (IEnumerable<TriangleData>)bf.Deserialize(new FileStream(fileName, FileMode.Open));
            return halpDataList;
        }
    }
}
