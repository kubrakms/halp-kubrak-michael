﻿// -----------------------------------------------------------------------
// <copyright file="XmlSerializer.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.HalpUtilitiesData.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;
    using System.Xml.Serialization;
    using System.Xml;
    using System.Runtime.Serialization;

    /// <summary>
    /// Implement ISerializer logic for xml format
    /// </summary>
    public class XMLSerializer : ISerializer
    {
        public void Serialize(IEnumerable<TriangleData> data, string fileName)
        {
            XmlSerializer xs = new XmlSerializer(typeof(List<TriangleData>), new XmlRootAttribute("Triangles"));
            using (XmlWriter xw = XmlWriter.Create(fileName))
            {
                xs.Serialize(xw, data);
            }
        }

        public IEnumerable<TriangleData> Deserialize(string fileName)
        {
            IEnumerable<TriangleData> halpDataList = default(IEnumerable<TriangleData>);
            XmlSerializer xs = new XmlSerializer(typeof(List<TriangleData>), new XmlRootAttribute("Triangles"));
            using (FileStream readFileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                halpDataList = (IEnumerable<TriangleData>)xs.Deserialize(readFileStream);
            }
            return halpDataList;
        }
    }
}
