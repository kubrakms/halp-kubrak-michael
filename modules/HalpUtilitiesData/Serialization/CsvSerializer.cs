﻿// -----------------------------------------------------------------------
// <copyright file="CsvSerializer.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.HalpUtilitiesData.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    /// <summary>
    /// Implement ISerializer logic for csv format
    /// </summary>
    public class CsvSerializer : ISerializer
    {
        public void Serialize(IEnumerable<TriangleData> data, string fileName)
        {
            using (StreamWriter sw = new StreamWriter(fileName,false,Encoding.UTF8))
            {
                foreach (var item in data)
                {
                    string csvString = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}",
                         item.Id.ToString(), item.Name.ToString(),
                         item.First.X.ToString(),
                         item.First.Y.ToString(),
                         item.First.Z.ToString(),
                         item.Second.X.ToString(),
                         item.Second.Y.ToString(),
                         item.Second.Z.ToString(),
                         item.Third.X.ToString(),
                         item.Third.Y.ToString(),
                         item.Third.Z.ToString());
                    sw.WriteLine(csvString);
                }
            }
        }

        public IEnumerable<TriangleData> Deserialize(string fileName)
        {
            List<TriangleData> halpDataList = new List<TriangleData>();
            string fileData = File.ReadAllText(fileName, Encoding.UTF8);
            foreach (var item in fileData.Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries))
            {

                string[] stringData = item.Split(new string[] { "\t" }, StringSplitOptions.RemoveEmptyEntries);
                TriangleData halpFormatData = new TriangleData()
                {
                    Id = Int32.Parse(stringData[0]),
                    Name = stringData[1],
                    First = new PointWrapper() { X = double.Parse(stringData[2]), Y = double.Parse(stringData[3]), Z = double.Parse(stringData[4]) },
                    Second = new PointWrapper() { X = double.Parse(stringData[5]), Y = double.Parse(stringData[6]), Z = double.Parse(stringData[7]) },
                    Third = new PointWrapper() { X = double.Parse(stringData[8]), Y = double.Parse(stringData[9]), Z = double.Parse(stringData[10]) },
                };
                halpDataList.Add(halpFormatData);
            }
            return halpDataList;
        }
    }
}
