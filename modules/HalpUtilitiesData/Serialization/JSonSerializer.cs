﻿// -----------------------------------------------------------------------
// <copyright file="JSonSerializer.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.HalpUtilitiesData.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Newtonsoft.Json;
    using System.IO;


    /// <summary>
    ///  Implement ISerializer logic for json format
    /// </summary>
    public class JSonSerializer : ISerializer
    {
        public void Serialize(IEnumerable<TriangleData> data, string fileName)
        {
            string serValue = JsonConvert.SerializeObject(data);
            using (StreamWriter sw = new StreamWriter(fileName))
            {
                sw.Write(serValue);
            }
        }

        public IEnumerable<TriangleData> Deserialize(string fileName)
        {
            string value;
            using (StreamReader sr = new StreamReader(fileName))
            {
                value = sr.ReadToEnd();
            }
            return (IEnumerable<TriangleData>)JsonConvert.DeserializeObject(value, typeof(IEnumerable<TriangleData>));
        }
    }
}
