﻿// -----------------------------------------------------------------------
// <copyright file="PointWrapper.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.HalpUtilitiesData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using geometry;
    using System.Xml.Serialization;

    /// <summary>
    /// Represent wrapper for Gm.Point class
    /// </summary>
    [Serializable]


    public class PointWrapper
    {

        #region [Constructors]

        public PointWrapper(Gm.Point point)
        {
            X = point.x;
            Y = point.y;
            Z = point.z;
        }
        public PointWrapper()
        {

        }

        #endregion  //[Constructors]

        #region [Porperties]
        /// <summary>
        /// Get or set X coordinate of point
        /// </summary>
        [XmlAttribute("X")]
        public double X { get; set; }
        /// <summary>
        /// Get or set Y coordinate of point
        /// </summary>
        [XmlAttribute("Y")]
        public double Y { get; set; }
        /// <summary>
        /// Get or set Z coordinate of point
        /// </summary>
        [XmlAttribute("Z")]
        public double Z { get; set; }

        #endregion // [Porperties]

    }
}
