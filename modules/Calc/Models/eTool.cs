﻿// -----------------------------------------------------------------------
// <copyright file="eTool.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.RunModule.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Represent eunmerable of 3d party tools
    /// </summary>
    public enum eTool:byte
    {
        fpFormat,
        fpTransform
    }
}
