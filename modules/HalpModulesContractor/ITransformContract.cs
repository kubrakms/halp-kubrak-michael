﻿// -----------------------------------------------------------------------
// <copyright file="ITransformContract.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.HalpModulesContractor
{
    using System;
    using System.Collections.Generic;
    using SoftServe.Halp.Modules.HalpUtilitiesData;
    

    /// <summary>
    /// Represents fpTransform utility behavior
    /// </summary>
    public interface ITransformContract : IDisposable
    {
        /// <summary>
        /// omputes triangle coordinates accourding new base point
        /// </summary>
        /// <param name="oldTriang">Old triangle</param>
        /// <param name="newTriang">New triangle</param>
        /// <exception cref="ArgumentException">If old triangle has not Id or no Name</exception>
        /// <exception cref="ArgumentNullException">If old triangle is Null</exception>
        void ComputeTriangle(TriangleData oldTriang, out TriangleData newTriang);

        /// <summary>
        /// Computes triangle coordinates accourding new base point in batch mode
        /// </summary>
        /// <param name="oldTriang">Old triangle set</param>
        /// <param name="newTriang">New triangle set</param>
        /// <exception cref="ArgumentException">If old triangle set has lements with invadid Id or no Name</exception>
        /// <exception cref="ArgumentNullException">If old triangle is Null</exception>
        void ComputeTriangleBath(IEnumerable<TriangleData> oldTriangSet, out IEnumerable<TriangleData> newTriangSet);
    }
}
