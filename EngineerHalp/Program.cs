﻿
namespace SoftServe.EngineerHalp
{

    using System;
    using System.Resources;
    using System.Threading;

   // using ViewModelResources = global::SoftServe.EngineerHalp.HalpResource;
    using System.Globalization;
    using SoftServe.EngineerHalp.Abstractions;
    using SoftServe.EngineerHalp.Controllers;
    using System.Configuration;

    class Program
    {
        static void Main(string[] args)
        {
            var culture = Thread.CurrentThread.CurrentCulture;

            using (var controller = new UserInputController(culture))
            {
                controller.Initialize();
                Console.WriteLine(controller.GetHelloString());
                Console.WriteLine(controller.GetHelp());


                while (!controller.IsExit)
                {
                    var command = controller.GetCommand();

                    if (string.Compare(command.Name, "Exit", true) == 0)
                    {
                        controller.IsExit = true;
                        continue;
                    }

                    if (string.Compare(command.Name, "create", true) == 0)
                    {
                        controller.CreateFile(command.Params[2]);
                        continue;
                    }

                    if (string.Compare(command.Name, "ver", true) == 0)
                    {
                        Console.WriteLine(controller.GetVersion());
                        continue;
                    }

                    if (string.Compare(command.Name, "help", true) == 0)
                    {
                        Console.WriteLine(controller.GetHelp());
                        continue;
                    }

                    if (string.Compare(command.Name, "transform", true) == 0)
                    {

                        Console.WriteLine(controller.GetTransform(command.Params[2], command.Params[3]));
                        continue;
                    }

                    if (string.Compare(command.Name, "format", true) == 0)
                    {
                        Console.WriteLine(controller.GetFormat(command.Params[2], command.Params[3], command.Params[4], command.Params[5]));
                        continue;
                    }
                }
            }
        }
    }
}
