﻿// -----------------------------------------------------------------------
// <copyright file="UserInputController.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.EngineerHalp.Abstractions;
    using System.Threading;
    using System.Globalization;
    using System.Resources;
    using SoftServe.EngineerHalp.Models;
    using System.IO;
    using SoftServe.Halp.Modules.RunModule.Controllers;
    using SoftServe.Halp.Modules.RunModule.Models;

    /// <summary>
    /// Implement logic for user input
    /// </summary>
    public class UserInputController : IUserInputContract
    {
        public UserInputController(CultureInfo culture)
        {
            mCulture = culture;
        }

        /// <summary>
        /// Initialize culture
        /// </summary>
        public void Initialize()
        {
            mResManager = new ResourceManager(typeof(global::SoftServe.EngineerHalp.Resources.HalpResources));
        }

        public bool IsExit { get; set; }

        public string GetHelp()
        {
            return mResManager.GetString("HelpString", mCulture);
        }

        public string GetHelloString()
        {
            return mResManager.GetString("HelloMessage", mCulture);
        }

        public string GetVersion()
        {
            return mResManager.GetString("Version", mCulture);
        }

        public void CreateFile(string name)
        {
            using (FileStream fs = File.Create(name)) { }
        }

        public int GetTransform(string input, string output)
        {
            using (ToolsRunController c = new ToolsRunController())
            {
                string parametrs = String.Format("{0} {1}", input, output);
                return c.RunTool(eTool.fpTransform , parametrs);
            }
        }

        public int GetFormat(string inputFile, string inputFormat, string outputFile, string outputFormat)
        {
            using (ToolsRunController c = new ToolsRunController())
            {
                string parametrs = String.Format("{0} {1} {2} {3}", inputFile, inputFormat, outputFile, outputFormat);
                return c.RunTool(eTool.fpFormat, parametrs);
            }
        }

        public Command GetCommand()
        {
            var line = Console.ReadLine();

            var command = new Command() { Name = line };
            IsExit = (string.Compare(command.Name, mResManager.GetString("Exit", mCulture), true) == 0);
            return command;
        }


        #region [IDispose Implementation]
        /// <summary>
        /// Implementation of IDispose
        /// </summary>
        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposeManaged)
        {
            if (mDisposed)
                return;

            mDisposed = true;
        }

        private bool mDisposed = false;

        #endregion //#region [IDispose Implementation]


        private readonly CultureInfo mCulture = new CultureInfo("en-US");

        private ResourceManager mResManager = default(ResourceManager);
    }
}
