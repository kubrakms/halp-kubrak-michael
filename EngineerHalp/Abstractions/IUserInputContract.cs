﻿// -----------------------------------------------------------------------
// <copyright file="IUserInputContract.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Abstractions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Represent logic for user input
    /// </summary>
    public interface IUserInputContract:IDisposable
    {
        /// <summary>
        /// Help for User
        /// </summary>
        /// <returns>help message</returns>
        string GetHelp();

        /// <summary>
        /// message for Hellp
        /// </summary>
        /// <returns>hello message</returns>
        string GetHelloString();

        /// <summary>
        /// Get version of programm
        /// </summary>
        /// <returns>string</returns>
        string GetVersion();

        /// <summary>
        /// Create empty file
        /// </summary>
        /// <param name="name">fileName</param>
        void CreateFile(string name);

        /// <summary>
        /// Execute fpTransform
        /// </summary>
        /// <param name="input">input file</param>
        /// <param name="output">output file</param>
        /// <returns> 0 if done, -21312 = if was troubles with utility</returns>
        int GetTransform(string input, string output);

        /// <summary>
        /// Execute fpFormat
        /// </summary>
        /// <param name="inputFile">input file</param>
        /// <param name="inputFormat">format of imput file</param>
        /// <param name="outputFile">output file</param>
        /// <param name="outputFormat">format of output file</param>
        /// <returns> 0 if done, -21312 = if was troubles with utility</returns>
        int GetFormat(string inputFile, string inputFormat, string outputFile, string outputFormat);

    }
}
