﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SoftServe.Halp.Modules.fpTransform.Abstractions;
using SoftServe.Halp.Modules.HalpUtilitiesData;
using SoftServe.Halp.Modules.HalpModulesContractor;
using SoftServe.Halp.Modules.HalpUtilitiesData.Enums;
using SoftServe.Halp.Modules.HalpUtilitiesData.Serialization;
using System.Text.RegularExpressions;
using System.IO;

namespace SoftServe.Halp.Modules.fpTransform
{
    class Program
    {
        static int Main(string[] args)
        {
            string inputFile = "input.xml";
            string outputfile = "output.xml";
            eFormatType formatType = eFormatType.XML;
            double coordinateX = 0;
            double coordinateY = 0;
            double coordinateZ = 0;
            int exitCode = 0;

            if (args.Length == 0)
            {
                using (IInputContract input = LogicContainer.Instance.GetService<IInputContract>())
                {
                    while (!input.IsDown)
                    {
                        Console.WriteLine(input.GetHelp());
                        if (!input.GetCoordinate("Enter X coordinate of point", ref coordinateX)) continue;
                        if (!input.GetCoordinate("Enter Y coordinate of point", ref coordinateY)) continue;
                        if (!input.GetCoordinate("Enter Z coordinate of point", ref coordinateZ)) continue;

                        using (ITransformContract transorm = LogicContainer.Instance.GetService<ITransformContract>())
                        {
                            try
                            {
                                var oldTriangle = new TriangleData() { First = new PointWrapper() { X = coordinateX, Y = coordinateY, Z = coordinateZ },Second = new PointWrapper(), Third = new PointWrapper() };
                                TriangleData newTirangle;
                                transorm.ComputeTriangle(oldTriangle, out newTirangle);
                                string resultString = string.Format("X={0}; Y={1}; Z={2}", newTirangle.First.X.ToString(), newTirangle.First.Y.ToString(), newTirangle.First.Z.ToString());
                                Console.WriteLine(resultString);
                            }
                            catch (Exception ex)
                            {
                                exitCode = -21312;
                            }
                        }
                    }
                }
            }
            else
            {
                if (!GetFormat(args[2], ref formatType)) { return -21312; }
                if (!GetInputFile(args[0], ref inputFile)) { return -21312; }
                if (!GetOutputFile(args[1], ref outputfile)) { return -21312; }

                ISerializer serializer = SerializerFactory.GetSerializer(formatType);
                IEnumerable<TriangleData> triangleList = serializer.Deserialize(inputFile);

                using (ITransformContract transorm = LogicContainer.Instance.GetService<ITransformContract>())
                {
                    try
                    {
                        IEnumerable<TriangleData> deserializedListOut;
                        transorm.ComputeTriangleBath(triangleList, out deserializedListOut);
                        if (deserializedListOut != null)
                            serializer.Serialize(deserializedListOut, outputfile);
                    }
                    catch (Exception ex)
                    {
                        exitCode = -21312;
                    }
                }
            }
            return exitCode;
        }

        private static bool GetFormat(string format, ref eFormatType result)
        {
            Regex r = new Regex("json|xml|csv|bin");
            if (!r.IsMatch(format))
                return false;
            else
                switch (format)
                {
                    case "json": { result = HalpUtilitiesData.Enums.eFormatType.JSON; break; }
                    case "xml": { result = HalpUtilitiesData.Enums.eFormatType.XML; break; }
                    case "csv": { result = HalpUtilitiesData.Enums.eFormatType.CSV; break; }
                    case "bin": { result = HalpUtilitiesData.Enums.eFormatType.BINARY; break; }
                }
            return true;
        }

        private static bool GetInputFile(string fileName, ref string result)
        {
            if (!File.Exists(fileName))
                return false;
            else
                result = fileName;
            return true;
        }

        private static bool GetOutputFile(string fileName, ref string result)
        {
            Regex r = new Regex("\\w.[a-z]*$");
            if (!r.IsMatch(fileName))
                return false;
            else
                result = fileName;
            return true;
        }
    }
}
