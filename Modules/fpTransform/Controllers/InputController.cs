﻿// -----------------------------------------------------------------------
// <copyright file="InputController.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.fpTransform.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.Halp.Modules.fpTransform.Abstractions;
    using System.IO;
    using System.Text.RegularExpressions;
    using SoftServe.Halp.Modules.HalpUtilitiesData.Enums;

    /// <summary>
    /// Represent class which implement IInputContract
    /// </summary>
    public class InputController : IInputContract
    {
        public bool GetCoordinate(string prompt, ref double resultCoordinate)
        {
            double resultValue = 0;
            bool isDone = false;
            do
            {
                Console.WriteLine(prompt + "\r\n");
                string value = Console.ReadLine();
                if ((string.Compare(value, "Exit", true) == 0))
                {
                    IsDown = true;
                    break;
                }
                if (double.TryParse(value, out resultValue))
                {
                    resultCoordinate = resultValue;
                    isDone = true;
                    return true;
                }
                else
                    Console.WriteLine("Entered value is not valid. Try again. Needed format example: 123.3212\r\n");

            } while (!isDone);
            return false; ;
        }

        public string GetHelp()
        {
            return "Enter point for transformation.\r\n Enter 'Exit' if you want exit\r\n";
        }

        public bool IsDown
        {
            get;
            private set;
        }

      

      

        public void Dispose()
        {

        }




    }
}
