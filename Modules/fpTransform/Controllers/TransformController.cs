﻿// -----------------------------------------------------------------------
// <copyright file="TransformController.cs" company="">
// general rule - no rules.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.fpTransform.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.Halp.Modules.fpTransform.Abstractions;
    using geometry;

    using SoftServe.Halp.Modules.HalpUtilitiesData;
    using SoftServe.Halp.Modules.HalpModulesContractor;

    /// <summary>
    /// Represent inplemantation for ITransformContract
    /// </summary>
    public class TransformController : ITransformContract
    {
        public void Dispose()
        {

        }

        public void ComputeTriangle(TriangleData oldTriang, out TriangleData newTriang)
        {
            Gm.Transform transform = new Gm.Transform(new double[] { 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 2 });
            Gm.Point pointResultFirst = transform.TransformPoint(new Gm.Point(oldTriang.First.X, oldTriang.First.Y, oldTriang.First.Z));
            Gm.Point pointResultSecond = transform.TransformPoint(new Gm.Point(oldTriang.Second.X, oldTriang.Second.Y, oldTriang.Second.Z));
            Gm.Point pointResultThird = transform.TransformPoint(new Gm.Point(oldTriang.Third.X, oldTriang.Third.Y, oldTriang.Third.Z));
            newTriang = new TriangleData()
            {
                Id = oldTriang.Id,
                Name = oldTriang.Name,
                First = new PointWrapper(pointResultFirst),
                Second = new PointWrapper(pointResultSecond),
                Third = new PointWrapper(pointResultThird)
            };
        }

        public void ComputeTriangleBath(IEnumerable<TriangleData> oldTriangSet, out IEnumerable<TriangleData> newTriangSet)
        {
            newTriangSet = new List<TriangleData>();
            foreach (var item in oldTriangSet)
            {
                TriangleData resTriangle;
                ComputeTriangle(item, out resTriangle);
                if (resTriangle != null)
                    (newTriangSet as List<TriangleData>).Add(resTriangle);
            }
        }

        public void ComputePoint(PointWrapper oldPoint, out PointWrapper newPoint)
        {
            Gm.Transform transform = new Gm.Transform(new double[] { 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 2 });
            newPoint =new PointWrapper(transform.TransformPoint(new Gm.Point(oldPoint.X, oldPoint.Y, oldPoint.Z)));
        }

    }
}
