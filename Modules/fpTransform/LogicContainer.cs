﻿// -----------------------------------------------------------------------
// <copyright file="HeadOfficeStorage.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.fpTransform
{
    using System;
    using System.Collections.Generic;

    using AutoMapper;
    
    using Autofac;
    using Autofac.Core;
    using Autofac.Builder;

    using SoftServe.Halp.Modules.fpTransform.Abstractions;
    using SoftServe.Halp.Modules.fpTransform.Controllers;
    using SoftServe.Halp.Modules.HalpModulesContractor;


    /// <summary>
    /// Provides service to creation custom modules with application's logic
    /// </summary>
    public class LogicContainer : IServiceProvider
    {
        static LogicContainer()
        {
            //HINT: aggregate complex the type converter via AutoMapper approach
            Mapper.CreateMap<object, Parameter>().ConvertUsing(new TypedParameterTypeConverter());

        }
        private LogicContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterInstance<ITransformContract>(new TransformController());
            builder.RegisterInstance<IInputContract>(new InputController());
            m_Container = builder.Build();
        }

        #region Nested private logic

        private class TypedParameterTypeConverter : ITypeConverter<object, TypedParameter>
        {
            public TypedParameter Convert(ResolutionContext context)
            {
                //if (context.SourceValue is IPathResolver)
                //    return new TypedParameter(typeof(IPathResolver), context.SourceValue);

                //if (context.SourceValue is IDistributedRepository)
                //    return new TypedParameter(typeof(IDistributedRepository), context.SourceValue);

                return new TypedParameter(context.SourceType, context.SourceValue);
            }
        }

        #endregion

        #region IServiceProvider implementation

        public object GetService(Type serviceType)
        {
            if (serviceType == typeof(ITransformContract))
                return m_Container.Resolve<ITransformContract>();
            if (serviceType == typeof(IInputContract))
                return m_Container.Resolve<IInputContract>();
           
            return null;
        }

        #endregion

        #region Thread safe singletone

        public static LogicContainer Instance
        {
            get
            {
                lock (__mutex)
                {
                    if (m_Instance == null)
                        m_Instance = new LogicContainer();
                }
                return m_Instance;
            }
        }

        #endregion

        public T GetService<T>()
        {
            return m_Container.Resolve<T>();
        }

        public T GetService<T>(object[] parameters)
        {
            if (parameters == null)
                return GetService<T>();

            var @params = Mapper.Map<object[], IEnumerable<TypedParameter>>(parameters);
            return m_Container.Resolve<T>(@params);
        }

        private static readonly object __mutex = new object();
        private static LogicContainer m_Instance = default(LogicContainer);

        private readonly IContainer m_Container = default(IContainer);
    }
}
