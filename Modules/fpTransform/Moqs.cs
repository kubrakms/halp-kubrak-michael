﻿// -----------------------------------------------------------------------
// <copyright file="TransformMoq.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.fpTransform
{
    using System;
    using System.IO;

    using SoftServe.Halp.Modules.fpTransform.Abstractions;

    using Moq;
    using SoftServe.Halp.Modules.HalpModulesContractor;


    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public static class Moqs
    {
        static Moqs()
        {
            try
            {
                Initalize();
            }
            catch (Exception)
            {
                /// TODO: Add log audit logic here
            }
        }

        public static ITransformContract TransformProxyMoq { get { return mTransformProxyMoq.Object; } }
        public static IInputContract InputProxyMoq { get { return mInputProxyMoq.Object; } }

        private static void Initalize()
        {
            lock (__mutex)
            {
                initializeTransform();
                initializeInput();
            }
        }

        private static void initializeTransform()
        {
           // mTransformProxyMoq.Setup(x => x.TrasformPoint(It.IsAny<PointWrapper>())).Returns<PointWrapper>((point) => new PointWrapper() { X = 1111, Y = 2222, Z = 3333 });
        }

        private static void initializeInput()
        {
            //mInputProxyMoq.Setup(x => x.GetHelp()).Returns("Some Help Message from Moq");
            //mInputProxyMoq.Setup(x => x.GetCoordinate(It.IsAny<string>())).Returns<string>((d) =>666);
        }

        private static readonly object __mutex = new object();
        private static readonly Mock<ITransformContract> mTransformProxyMoq = new Mock<ITransformContract>(MockBehavior.Loose);
        private static readonly Mock<IInputContract> mInputProxyMoq = new Mock<IInputContract>(MockBehavior.Loose);
    }
}
