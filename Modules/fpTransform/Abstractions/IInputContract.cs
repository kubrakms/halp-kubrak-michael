﻿// -----------------------------------------------------------------------
// <copyright file="IInputContract.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.fpTransform.Abstractions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
using SoftServe.Halp.Modules.HalpUtilitiesData.Enums;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IInputContract:IDisposable
    {
        /// <summary>
        /// true if was eneter exit command
        /// </summary>
        bool IsDown { get;  }


        /// <summary>
        /// Get value for coordinate
        /// </summary>
        /// <param name="value">user input value</param>
        /// <returns>parsed value</returns>
        /// <exception cref="ArgumentException"></exception>
        bool GetCoordinate(string prompt,ref double resultCoordinate);
        /// <summary>
        /// method for getting help
        /// </summary>
        /// <returns>some help message</returns>
         string GetHelp();
    }
}
