﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SoftServe.Halp.Modules.HalpUtilitiesData.Serialization;
using SoftServe.Halp.Modules.HalpModulesContractor;
using SoftServe.Halp.Modules.HalpUtilitiesData;
using SoftServe.Halp.Modules.fpFormat.Abstractions;
using SoftServe.Halp.Modules.fpFormat.Controllers;
using System.Net;
namespace SoftServe.Halp.Modules.fpFormat
{
    class Program
    {
        static int Main(string[] args)
        {
            try
            {
                string inputFile = default(string);
                string inputFormat = default(string);
                string outputFile = default(string);
                string outputFormat = default(string);
                string logFile = default(string);
                //package mode
                if (args.Length != 0)
                {
                    //parse args
                    if (args[0].Contains("in:"))
                        inputFile = args[0].Substring(3);
                    if (args[1].Contains("from:"))
                        inputFormat = args[1].Substring(5);
                    if (args[2].Contains("out:"))
                        outputFile = args[2].Substring(4);
                    if (args[3].Contains("to:"))
                        outputFormat = args[3].Substring(3);
                    if (args[4].Contains("l:"))
                        logFile = args[4].Substring(2);
                }
                else
                {
                    using (IInputContract inputContract = LogicContainer.Instance.GetService<IInputContract>())
                    {
                        while (!inputContract.IsDown)
                        {
                            if (!inputContract.GetLogFile("Exit", ref logFile)) continue;
                            if (!inputContract.GetInputFile("Exit", ref inputFile)) continue;
                            if (!inputContract.GetInputFormat("Exit", ref inputFormat)) continue;
                            if (!inputContract.GetOutputFile("Exit", ref outputFile)) continue;
                            if (!inputContract.GetOutputFOrmat("Exit", ref outputFormat)) continue;
                           
                            break;
                        }
                    }
                }
                using (IFormatContract formatInstance = LogicContainer.Instance.GetService<IFormatContract>())
                {
                    var collection = formatInstance.ReadFormat(inputFile, formatInstance.DetectFormatTypeAtExt(inputFormat));
                    formatInstance.WriteFormat(collection, formatInstance.DetectFormatTypeAtExt(outputFormat), outputFile);
                }
            }
            catch (Exception ex)
            {

                return -21312;
            }
            return 0;
        }
    }
}
