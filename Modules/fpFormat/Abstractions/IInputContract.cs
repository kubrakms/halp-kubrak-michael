﻿// -----------------------------------------------------------------------
// <copyright file="IInputContract.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.fpFormat.Abstractions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using geometry;


    /// <summary>
    /// Represent input logic
    /// </summary>
    public interface IInputContract : IDisposable
    {
        /// <summary>
        /// When true than program exit
        /// </summary>
        bool IsDown { get; }
        /// <summary>
        /// get input file
        /// </summary>
        /// <param name="exitCommand">if exit than Exit from utility</param>
        /// <param name="resultData">returning data</param>
        /// <returns>if work done return true</returns>
        bool GetInputFile(string exitCommand, ref string resultData);

        /// <summary>
        /// get Input Format
        /// </summary>
        /// <param name="exitCommand">if exit than Exit from utility</param>
        /// <param name="resultData">returning queriable  data</param>
        /// <returns>if work done return true</returns>
        bool GetInputFormat(string exitCommand, ref string resultData);

        /// <summary>
        /// get output file
        /// </summary>
        /// <param name="exitCommand">if exit than Exit from utility</param>
        /// <param name="resultData">returning data</param>
        /// <returns>if work done return true</returns>
        bool GetOutputFile(string exitCommand, ref string resultData);

        /// <summary>
        /// get Output Format
        /// </summary>
        /// <param name="exitCommand">if exit than Exit from utility</param>
        /// <param name="resultData">returning queriable  data</param>
        /// <returns>if work done return true</returns>
        bool GetOutputFOrmat(string exitCommand, ref string resultData);

        /// <summary>
        /// get LogFile
        /// </summary>
        /// <param name="exitCommand">if exit than Exit from utility</param>
        /// <param name="resultData">returning queriable  data</param>
        /// <returns>if work done return true</returns>
        bool GetLogFile(string exitCommand, ref string resultData);
    }
}
