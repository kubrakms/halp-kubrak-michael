﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Halp.Modules.fpFormat.Enums
{

    /// <summary>
    /// Represent commands enumeration
    /// </summary>
    public enum eCommandType:byte
    {
        InputFolder = 0,
        OutputFolder = 1,
        InputFormat = 2,
        OutputFormat = 3,
    }
}
