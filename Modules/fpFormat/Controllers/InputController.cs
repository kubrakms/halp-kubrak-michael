﻿// -----------------------------------------------------------------------
// <copyright file="InputContract.cs" company="">
//general rule = no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.fpFormat.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
   
    using System.IO;
    using System.Text.RegularExpressions;
    using SoftServe.Halp.Modules.fpFormat.Abstractions;

    /// <summary>
    /// Represent input logic
    /// </summary>
    public class InputController : IInputContract
    {

        public bool IsDown
        {
            get;
            private set;
        }

        public bool GetInputFile(string exitCommand, ref string resultData)
        {
            bool isExitState = false;
            do
            {
                Console.WriteLine("Enter input file. \r\n");
                resultData = Console.ReadLine();
                IsDown = (string.Compare(exitCommand, resultData, true) == 0);
                if (File.Exists(resultData))
                    isExitState = true;
                else
                {
                    Console.WriteLine("File {0} not find.", resultData);
                    LogHelper.WriteInfo(string.Format("File {0} not find.", resultData));
                }
            } while (!isExitState);
            return true;
        }

        public bool GetInputFormat(string exitCommand, ref string resultData)
        {
            bool isExitState = false;
            do
            {
                Console.WriteLine("Enter input format. \r\n");
                resultData = Console.ReadLine();
                IsDown = (string.Compare(exitCommand, resultData, true) == 0);
                Regex r = new Regex("bin|json|xml|csv");
                if (r.IsMatch(resultData))
                    isExitState = true;
                else
                    Console.WriteLine("Wrong data format. Please write next format: json, xml, bin, csv \r\n");
            } while (!isExitState);
            return true;
        }

        public bool GetOutputFile(string exitCommand, ref string resultData)
        {

            bool isExitState = false;
            do
            {
                Console.WriteLine("Enter output file. \r\n");
                resultData = Console.ReadLine();
                IsDown = (string.Compare(exitCommand, resultData, true) == 0);
                if (!File.Exists(resultData))
                    using (File.Create(resultData)) { }
                isExitState = true;
            } while (!isExitState);
            return true;
        }

        public bool GetOutputFOrmat(string exitCommand, ref string resultData)
        {
            bool isExitState = false;
            do
            {
                Console.WriteLine("Enter output format. \r\n");
                resultData = Console.ReadLine();
                IsDown = (string.Compare(exitCommand, resultData, true) == 0);
                Regex r = new Regex("bin|json|xml|csv");
                if (r.IsMatch(resultData))
                    isExitState = true;
            } while (!isExitState);
            return true;
        }

        public bool GetLogFile(string exitCommand, ref string resultData)
        {
            bool isExitState = false;
            do
            {
                Console.WriteLine("Enter log file. \r\n");
                resultData = Console.ReadLine();
                IsDown = (string.Compare(exitCommand, resultData, true) == 0);
                if (!File.Exists(resultData))
                    using (File.Create(resultData)) { }
                isExitState = true;
            } while (!isExitState);
            LogHelper.SetLogFile(resultData);
            return true;
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposeManaged)
        {
            if (mDisposed)
                return;

            mDisposed = true;
        }

        private bool mDisposed = false;
    }
}
