﻿// -----------------------------------------------------------------------
// <copyright file="FormatController.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.fpFormat.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using System.Xml.Serialization;
    using SoftServe.Halp.Modules.HalpUtilitiesData.Enums;
    using SoftServe.Halp.Modules.HalpUtilitiesData.Serialization;
    using SoftServe.Halp.Modules.HalpUtilitiesData;
    using SoftServe.Halp.Modules.HalpModulesContractor;

    /// <summary>
    /// Implement logic of IFormatContract contract
    /// </summary>
    public class FormatController : IFormatContract
    {
        private readonly Dictionary<string, eFormatType> FormatTypeDictionary;

        public FormatController()
        {
            FormatTypeDictionary = new Dictionary<string, eFormatType>();
            FormatTypeDictionary.Add("xml", eFormatType.XML);
            FormatTypeDictionary.Add("json", eFormatType.JSON);
            FormatTypeDictionary.Add("csv", eFormatType.CSV);
            FormatTypeDictionary.Add("bin", eFormatType.BINARY);
        }

        public IEnumerable<TriangleData> ReadFormat(string sourceFile, eFormatType typeSource)
        {
            ISerializer serializer = SerializerFactory.GetSerializer(typeSource);
            IEnumerable<TriangleData> triangleCollection = serializer.Deserialize(sourceFile);
            return triangleCollection;
        }

        public void WriteFormat(IEnumerable<TriangleData> data, eFormatType typeDest, string destFile)
        {
            ISerializer serializer = SerializerFactory.GetSerializer(typeDest);
            serializer.Serialize(data, destFile);
        }

        public eFormatType DetectFormatTypeAtExt(string sourceFile)
        {
            eFormatType fType = eFormatType.None;
            if(FormatTypeDictionary.ContainsKey(sourceFile))
                fType = FormatTypeDictionary[sourceFile.ToLower()];
            else
                LogHelper.WriteError("entered bad value for format.");
            return fType;
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposeManaged)
        {
            if (mDisposed)
                return;

            mDisposed = true;
        }

        private bool mDisposed = false;


    }
}
