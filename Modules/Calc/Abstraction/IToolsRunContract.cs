﻿// -----------------------------------------------------------------------
// <copyright file="ICalculatorContract.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.RunModule.Abstraction
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.Halp.Modules.RunModule.Models;


    /// <summary>
    /// Represent logic for run 3d-Tools
    /// </summary>
    public interface IToolsRunContract : IDisposable
    {
        /// <summary>
        /// Run concreate tool by eTool type
        /// </summary>
        /// <param name="tool">tool type </param>
        /// <param name="args"> arguments for tool</param>
        /// <returns>exit code of tool</returns>
        int RunTool(eTool tool,string args);
    }
}
