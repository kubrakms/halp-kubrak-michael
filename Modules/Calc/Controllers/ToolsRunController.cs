﻿// -----------------------------------------------------------------------
// <copyright file="CalculatorController.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.RunModule.Controllers
{
    using System;
    using SoftServe.Halp.Modules.RunModule.Abstraction;
    using SoftServe.Halp.Modules.RunModule.Models;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;


    /// <summary>
    /// Implement logic of IToolsRunContract for running 3d-tools.
    /// </summary>
    public class ToolsRunController : IToolsRunContract
    {
        #region [Fields]
        private readonly Dictionary<eTool, string> ToolsDictionary = new Dictionary<eTool, string>();
        #endregion // [Fields]

        #region [Properties]
        public ToolsRunController()
        {
            ToolsDictionary.Add(eTool.fpFormat, "fpFormat.exe");
            ToolsDictionary.Add(eTool.fpFormat, "fpTransform.exe");
        }
        #endregion // [Properties]

        #region [IDispose Implementation]

        /// <summary>
        /// Implementation of IDispose interface
        /// </summary>
        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposeManaged)
        {
            if (mDisposed)
                return;

            mDisposed = true;
        }

        private bool mDisposed = false;

        #endregion // [IDispose Implementation]

        #region [IToolsRunContract Implementation]
        
        public int RunTool(eTool tool, string args)
        {
            var startInfo = new ProcessStartInfo(ToolsDictionary[tool], args)
            {
                CreateNoWindow = true,
                UseShellExecute = true
            };
            using (var process = Process.Start(startInfo))
            {
                process.WaitForExit();
                return process.ExitCode;
            }
        }
        #endregion // [IToolsRunContract Implementation]
    }
}
