﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Halp.Modules.HalpUtilitiesData.Enums
{
    public enum eFormatType : byte
    {
        None = 0,
        XML = 1,
        JSON = 2,
        CSV = 3,
        BINARY = 4
    }
}
