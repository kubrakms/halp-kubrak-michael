﻿// -----------------------------------------------------------------------
// <copyright file="SerializerFactory.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.HalpUtilitiesData.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.Halp.Modules.HalpUtilitiesData.Enums;

    /// <summary>
    /// Factory method pattern for getting instance of serializator
    /// </summary>
    public class SerializerFactory
    {
        public static ISerializer GetSerializer(eFormatType serializerType)
        {
            switch (serializerType)
            {
                case  eFormatType.XML:
                    {
                        return new XMLSerializer();
                    }
                case eFormatType.JSON:
                    {
                        return new JSonSerializer();
                    }
                case eFormatType.BINARY:
                    {
                        return new BinarySerializer();
                    }
                case eFormatType.CSV:
                    {
                        return new CsvSerializer();
                    }
                default:
                    {
                        return new XMLSerializer();
                    }
            }
        }
    }
}
