﻿// -----------------------------------------------------------------------
// <copyright file="ISerializer.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.HalpUtilitiesData.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;


    /// <summary>
    /// Represent Logic of Serialization
    /// </summary>
    public interface ISerializer
    {
        void Serialize(IEnumerable<TriangleData> data, string fileName);
        IEnumerable<TriangleData> Deserialize(string fileName);

    }
}
