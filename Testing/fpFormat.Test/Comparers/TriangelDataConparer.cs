﻿// -----------------------------------------------------------------------
// <copyright file="TriangelDataConparer.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.fpFormat.Test.Comparers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.Halp.Modules.HalpUtilitiesData;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class TriangelDataConparer:IComparer<TriangleData>
    {
        public int Compare(TriangleData real, TriangleData etalon)
        {
            if (real.First.X != etalon.First.X) return -1;
            if (real.First.Y != etalon.First.Y) return -1;
            if (real.First.Z != etalon.First.Z) return -1;

            if (real.Second.X != etalon.Second.X) return -1;
            if (real.Second.Y != etalon.Second.Y) return -1;
            if (real.Second.Z != etalon.Second.Z) return -1;

            if (real.Third.X != etalon.Third.X) return -1;
            if (real.Third.Y != etalon.Third.Y) return -1;
            if (real.Third.Z != etalon.Third.Z) return -1;

            if (real.Id != etalon.Id) return -1;

            if (string.Compare(real.Name, etalon.Name, true) != 0) return -1;

            return 0;

        }
    }
}
