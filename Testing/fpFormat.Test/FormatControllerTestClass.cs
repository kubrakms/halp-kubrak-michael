﻿// -------------------------------------------------------------- ---------
// <copyright file="FOrmatControllerTestClass.cs" company="">
// general rule  - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.fpFormat.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using NUnit.Framework;
    using SoftServe.Halp.Modules.HalpUtilitiesData;
    using SoftServe.Halp.Modules.HalpModulesContractor;
    using SoftServe.Halp.Modules.HalpUtilitiesData.Enums;
    using System.Security.Cryptography;
    using System.IO;
    using SoftServe.Halp.Modules.fpFormat.Test.Comparers;

    /// <summary>
    /// Test class fo FormatController
    /// </summary>
    [TestFixture]
    public class FormatControllerTestClass
    {
        public IEnumerable<TestCaseData> GetTestList
        {
            get
            {
                var list = new List<TriangleData>();
                for (int i = 01; i < 5; i++)
                {
                    list.Add(new TriangleData()
                    {
                        First = new PointWrapper(new geometry.Gm.Point(1, 1, 1)),
                        Second = new PointWrapper(new geometry.Gm.Point(1, 1, 1)),
                        Third = new PointWrapper(new geometry.Gm.Point(1, 1, 1)),
                        Id = i,
                        Name = "Name" + i.ToString()
                    }
                    );
                }
                yield return new TestCaseData(list, eFormatType.XML, "Test.xml", "TestData/Read/Xml.xml");

            }
        }

        public IEnumerable<ITestCaseData> GetTestListForReadAction
        {
            get 
            {
                var list = new List<TriangleData>();
                for (int i = 01; i < 5; i++)
                {
                    list.Add(new TriangleData()
                    {
                        First = new PointWrapper(new geometry.Gm.Point(1, 1, 1)),
                        Second = new PointWrapper(new geometry.Gm.Point(1, 1, 1)),
                        Third = new PointWrapper(new geometry.Gm.Point(1, 1, 1)),
                        Id = i,
                        Name = "Name" + i.ToString()
                    }
                    );
                }
                yield return new TestCaseData(list, eFormatType.XML, "TestData/Read/Xml.xml");
            }
        }

        [TestCaseSource("GetTestListForReadAction")]
        public void FormatControllerReadFromFile_Action(IEnumerable<TriangleData> testcollection, eFormatType type, string fileName)
        {

            List<TriangleData> testList = new List<TriangleData>(testcollection);
            using (IFormatContract formatContract = LogicContainer.Instance.GetService<IFormatContract>())
            {
                var result = formatContract.ReadFormat(fileName, type);
                Assert.IsNotNull(result);

                List<TriangleData> readedData = new List<TriangleData>(result);
                Assert.IsTrue(testList.Count() == readedData.Count());

                TriangelDataConparer comparer = new TriangelDataConparer();
                for (int i = 0; i < testList.Count(); i++)
                    Assert.IsTrue(comparer.Compare(testList[i], readedData[i]) == 0);
            }
        }

        [TestCaseSource("GetTestList")]
        public void FormatControllerWriteToFile_Action(IEnumerable<TriangleData> testList, eFormatType type, string fileName, string etalonFile)
        {
            using (IFormatContract format = LogicContainer.Instance.GetService<IFormatContract>())
            {
                format.WriteFormat(testList, type, fileName);
            }
            var md5Provider = new MD5CryptoServiceProvider();
            using (var et = new FileStream(etalonFile, FileMode.Open))
            {
                using (var actual = new FileStream(fileName, FileMode.Open))
                {
                    Assert.AreEqual(md5Provider.ComputeHash(et), md5Provider.ComputeHash(actual));
                }
            }
        }

        [TestCase("json", Result = eFormatType.JSON)]
        [TestCase("xml", Result = eFormatType.XML)]
        [TestCase("csv", Result = eFormatType.CSV)]
        [TestCase("bin", Result = eFormatType.BINARY)]
        [TestCase("someFalseValue", Result = eFormatType.None)]
        public eFormatType DetectFormatTypeAtExt_Action(string value)
        {
            using (IFormatContract fContract = LogicContainer.Instance.GetService<IFormatContract>())
            {
                return fContract.DetectFormatTypeAtExt(value);
            }
        }
    }
}
