﻿// -----------------------------------------------------------------------
// <copyright file="UserInputControllerTestClass.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Testing.EngineerHalp.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using NUnit.Framework;
    using SoftServe.EngineerHalp;
    using SoftServe.EngineerHalp.Abstractions;
    using SoftServe.EngineerHalp.Controllers;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [TestFixture]
    public class UserInputControllerTestClass
    {
        [TestCase("HelloMessage", "en-Us", Result = "Hello user. Input command please.")]
        [TestCase("HelloMessage", "ru-Ru", Result = "Приветствую юзверь. Вы можете ввести команды:")]
        public string LocalizationTest_enUS_HelloMessageMethod(string neddedstring, string cultureName)
        {
            using (var controller = new UserInputController(new System.Globalization.CultureInfo(cultureName)))
            {
                controller.Initialize();
                return controller.GetHelloString();
            }
        }
    }
}
