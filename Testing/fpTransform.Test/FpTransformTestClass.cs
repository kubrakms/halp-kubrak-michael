﻿// -----------------------------------------------------------------------
// <copyright file="FpTransformTestClass.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Testing.fpTransform.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.Halp.Modules.HalpUtilitiesData;
    using NUnit.Framework;
    using SoftServe.Halp.Modules.HalpModulesContractor;
    using SoftServe.Halp.Modules.fpTransform;
    using SoftServe.Halp.Testing.fpTransform.Test.Comparers;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class FpTransformTestClass
    {

        public IEnumerable<TestCaseData> GetDataForComputeTriangleAction
        {
            get
            {
                var resultData = new TriangleData()
                {
                    Id = 1,
                    Name = "Name1",
                    First = new PointWrapper() { X = 2, Y = 3, Z = 4 },
                    Second = new PointWrapper() { X = 2, Y = 3, Z = 4 },
                    Third = new PointWrapper() { X = 2, Y = 3, Z = 4 },
                };
                var enterdData = new TriangleData()
                {
                    Id = 1,
                    Name = "Name1",
                    First = new PointWrapper() { X = 1, Y = 2, Z = 3 },
                    Second = new PointWrapper() { X = 1, Y = 2, Z = 3 },
                    Third = new PointWrapper() { X = 1, Y = 2, Z = 3 },
                };
                yield return new TestCaseData(enterdData, resultData);
            }
        }

        public IEnumerable<TestCaseData> GetDataForComputeTriangleActionList
        {
            get
            {
                List<TriangleData> enteredList = new List<TriangleData>();
                for (int i = 0; i < 5; i++)
                {
                    enteredList.Add(new TriangleData()
                    {
                        Id = 1,
                        Name = "Name1",
                        First = new PointWrapper() { X = 1, Y = 2, Z = 3 },
                        Second = new PointWrapper() { X = 1, Y = 2, Z = 3 },
                        Third = new PointWrapper() { X = 1, Y = 2, Z = 3 },
                    });
                }
                List<TriangleData> resultList = new List<TriangleData>();
                for (int i = 0; i < 5; i++)
                {
                    resultList.Add(new TriangleData()
                        {
                            Id = 1,
                            Name = "Name1",
                            First = new PointWrapper() { X = 2, Y = 3, Z = 4 },
                            Second = new PointWrapper() { X = 2, Y = 3, Z = 4 },
                            Third = new PointWrapper() { X = 2, Y = 3, Z = 4 },
                        });
                }
                yield return new TestCaseData(enteredList, resultList);
            }
        }


        [TestCaseSource("GetDataForComputeTriangleAction")]
        public void TransformControllerComputeTriangle_Action(TriangleData enterdData, TriangleData etalontData)
        {
            using (ITransformContract transform = LogicContainer.Instance.GetService<ITransformContract>())
            {
                TriangleData resultData;
                transform.ComputeTriangle(enterdData, out resultData);
                TriangleDataComparer comparer = new TriangleDataComparer();
                Assert.IsTrue(comparer.Compare(resultData, etalontData) == 0);
            }
        }


        [TestCaseSource("GetDataForComputeTriangleActionList")]
        public void TransformControllerComputeTriangleBath_Action(IEnumerable<TriangleData> enterdData, List<TriangleData> etalonData)
        {
            using (ITransformContract transform = LogicContainer.Instance.GetService<ITransformContract>())
            {   
                IEnumerable<TriangleData> resultCollection;
                transform.ComputeTriangleBath(enterdData, out resultCollection);

                Assert.IsNotNull(resultCollection);
                Assert.IsTrue(resultCollection.Count() == etalonData.Count());
                List<TriangleData> resiltList = new List<TriangleData>(resultCollection);
                TriangleDataComparer comparer = new TriangleDataComparer();
                for (int i = 0; i < resiltList.Count(); i++)
                {
                    Assert.IsTrue(comparer.Compare(etalonData[i], resiltList[i]) == 0);
                }
            }
        }
    }
}
