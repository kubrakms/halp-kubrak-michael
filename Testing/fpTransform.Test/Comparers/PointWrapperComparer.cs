﻿// -----------------------------------------------------------------------
// <copyright file="PointWrapperComparer.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Testing.fpTransform.Test.Comparers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.Halp.Modules.HalpUtilitiesData;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class PointWrapperComparer : IComparer<PointWrapper>
    {
        public int Compare(PointWrapper oldPoint, PointWrapper newPoint)
        {
            if (oldPoint.X != newPoint.X) return -1;
            if (oldPoint.Y != newPoint.Y) return -1;
            if (oldPoint.X != newPoint.X) return -1;

            return 0;
        }
    }
}
