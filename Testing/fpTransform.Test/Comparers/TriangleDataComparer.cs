﻿// -----------------------------------------------------------------------
// <copyright file="TriangleDataComparer.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Testing.fpTransform.Test.Comparers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.Halp.Modules.HalpUtilitiesData;

    /// <summary>
    /// Represent Class with test methods for TriangleComparer
    /// </summary>
    public class TriangleDataComparer : IComparer<TriangleData>
    {
        public int Compare(TriangleData oldTriangle, TriangleData newTriangle)
        {
            if (oldTriangle.First.X != newTriangle.First.X) return -1;
            if (oldTriangle.First.Y != newTriangle.First.Y) return -1;
            if (oldTriangle.First.Z != newTriangle.First.Z) return -1;

            if (oldTriangle.Second.X != newTriangle.Second.X) return -1;
            if (oldTriangle.Second.Y != newTriangle.Second.Y) return -1;
            if (oldTriangle.Second.Z != newTriangle.Second.Z) return -1;

            if (oldTriangle.Third.X != newTriangle.Third.X) return -1;
            if (oldTriangle.Third.Y != newTriangle.Third.Y) return -1;
            if (oldTriangle.Third.Z != newTriangle.Third.Z) return -1;

            if (oldTriangle.Id != newTriangle.Id) return -1;

            if (string.Compare(oldTriangle.Name, newTriangle.Name, true) != 0) return -1;

            return 0;
        }
    }
}
